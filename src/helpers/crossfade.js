import { crossfade, fade } from 'svelte/transition';
export default crossfade({
    duration: 200,
    fallback: fade
});