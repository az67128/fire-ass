const functions = require("firebase-functions");
const nodemailer = require("nodemailer");
const cors = require("cors")({
  origin: true,
});

const admin = require("firebase-admin");
admin.initializeApp();

exports.readArticle = functions.https.onRequest(async (req, res) => {
  const id = req.query.id;
  const articleRef = await admin.firestore().collection("articles").doc(id);
  articleRef.set(
    { reads: admin.firestore.FieldValue.increment(1) },
    { merge: true }
  );
  return cors(req, res, () => {
    res.json({ result: `ok` });
  });
});

exports.likeArticle = functions.https.onRequest(async (req, res) => {
  const id = req.query.id;
  const articleRef = await admin.firestore().collection("articles").doc(id);
  articleRef.set(
    { likes: admin.firestore.FieldValue.increment(1) },
    { merge: true }
  );
  return cors(req, res, () => {
    res.json({ result: `ok` });
  });
});

exports.getArticle = functions.https.onRequest(async (req, res) => {
  const id = req.query.id;
  const articleRef = await admin.firestore().collection("articles").doc(id);
  const doc = await articleRef.get();
  return cors(req, res, () => {
    res.json({ result: doc.exists ? doc.data() : {} });
  });
});

exports.getArticles = functions.https.onRequest(async (req, res) => {
  const articlesRef = await admin.firestore().collection("articles");
  const snapshot = await articlesRef.get();
  const result = {};
  if (!snapshot.empty) {
    snapshot.forEach((doc) => {
      result[doc.id] = doc.data();
    });
  }
  return cors(req, res, () => {
    res.json({ result });
  });
});

exports.writeUs = functions.https.onRequest(async (req, res) => {
  if (req.method !== "POST") {
    res.send(405, "HTTP Method " + req.method + " not allowed");
    return;
  }
  const body = JSON.parse(req.body);
  if (!body.text || !body.email) {
    res.send(405, "Bad request");
    return;
  }

  const configRef = await admin.firestore().collection("config").doc("mail");
  const emailConfigDoc = await configRef.get();
  const config = emailConfigDoc.data();

  const mailTransport = nodemailer.createTransport({
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: config.login,
      pass: config.password,
    },
  });

  const mailOptions = {
    from: '"Профессиональное пригорание" <az67128@yandex.ru>',
    to: config.distributionList,
    subject: "У кого-то пригорело!",
    text: `От: ${body.email}\n${body.text}`,
  };
  try {
    await mailTransport.sendMail(mailOptions);
  } catch (error) {
    console.error("There was an error while sending the email:", error);
  }
  cors(req, res, () => {
    res.json({ result: "ok" });
  });
});
